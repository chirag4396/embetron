<?php
        include'dbconnect.php';
        session_start();
        if((!isset($_SESSION['admin']) || $_SESSION['admin']!='Admin'))
        {
             echo"<script> window.location='index.php' ; </script>";
        }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Contacts</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="imgupload.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php 
                                        echo $_SESSION['admin'];
                                       ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="../../img/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php 
                                        echo $_SESSION['admin'];
                                       ?> 
                                    </p>
                                </li>
                                <!-- Menu Body -->
                               
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                   
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php 
                                        echo $_SESSION['admin'];
                                       ?> <i class="caret"></i></span></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                         <li >
                            <a href="imgupload.php">
                                <i class="fa fa-dashboard"></i> <span>Gallery</span>
                            </a>
                        </li>
                        <li >
                            <a href="product-upload.php">
                                <i class="fa fa-dashboard"></i> <span>product</span>
                            </a>
                        </li>
                        <li >
                            <a href="admincareers.php">
                                <i class="fa fa-th"></i> <span>Careers</span> 
                            </a>
                        </li>
                        <li class="active">
                            <a href="admincontact.php">
                                <i class="fa fa-th"></i> <span>Contacts</span> 
                            </a>
                        </li>
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Display All Contacts Request
                       
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">General Elements</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-12">
                                         

                                            <div  name="successmsg" id="successmsg"  style="color: green;margin-left: 20px;" > </div>
                                       
                                    </div>
                        <div class="col-xs-12">
                            <div class="box">
                                
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>email</th>
                                                <th>Subject</th>
                                                <th>message</th>
                                                 <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?PHP 
                                                $sql ="select id,name,email,subject,msg from contact";
                                                $res =mysqli_query($con,$sql)or die("select-error ".mysqli_error($con));
                                                while($row = mysqli_fetch_array($res))
                                                {
                                                    $id=$row['id'];
                                            ?>
                                                <tr>
                                                <td><?php echo $id;  ?></td>
                                                <td><?php echo $row['name'];  ?></td>
                                                <td><?php echo $row['email'];  ?> </td>
                                                 <td><?php echo $row['subject'];  ?> </td>
                                                 <td><?php echo $row['msg'];  ?> </td>
                                    <td><?php echo'<button class="btn-danger"  id='.$id.'  onclick="deleterec('.$id.')">Delete</button>';?></td>
                                               
                                            </tr>

                                            <?php 
                                                }
                                            ?>
                                           
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
         <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
         <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                
            });

            function deleterec(id)
        {
            var id= id;
            //alert(id);
            var str = "id="+id+"&status=deletecontact";
            //alert(str);
            $.ajax({ type:'post',data:str,url:'ajaximgupload.php',success:function(html)
                {

                    //alert(html);
                    document.getElementById("successmsg").innerHTML=html;
                }

            });
            
                 setTimeout(function() {
                  $('#successmsg').fadeOut('fast');
                }, 3000); 

                var res = timedRefresh(3000);
            if(res==false)
            return false; 
}

        function timedRefresh(timeoutPeriod) {
             setTimeout("location.reload(true);",timeoutPeriod);
        }
        </script>
    </body>
</html>