<?php
        include'dbconnect.php';
        session_start();
        if((!isset($_SESSION['admin']) || $_SESSION['admin']!='Admin'))
        {
             echo"<script> window.location='index.php' ; </script>";
        }
$error = "";
$error1 = "";
$success="";
$f=0;
$f1=0;
        if(isset($_POST['submit']))
        {
            if (isset($_FILES["imgupload"])) 
            {
                 $allowedExts = array("jpg","png","jpeg");
                 $temp = explode(".", $_FILES["imgupload"]["name"]);
                 $extension = end($temp);
                 
                 if ($_FILES["imgupload"]["error"] > 0) {
                     $error .= "Invalid Image Format<br />";
                 }
                 if (!in_array($extension, $allowedExts)) {
                    $error .= "  Image should be .png .jpg .jpeg Extension  <br />";
                 }
                 if ($_FILES["imgupload"]["size"] > 2000000) {
                    $error .= "image size shoud be less than 2 Mb<br />";
                 }
                 
                 if ($error == "") 
                 {
                    $f=1;
                 }
                  else 
                 {
                     $error;
                 }
            }
            if (isset($_FILES["pdfupload"])) 
            {  
                 $allowedExts = array("pdf");
                  $temp = explode(".", $_FILES["pdfupload"]["name"]);
                 $extension = end($temp);
                 

                 if (!in_array($extension, $allowedExts)) {
                    $error1 .= "  Only .pdf Extension allowed <br />";
                 }
                 if ($_FILES["pdfupload"]["size"] > 2000000) {
                    $error1 .= "File size shoud be less than 2 Mb<br />";
                 }
                 
                 if ($error1 == "") 
                 {
                    $f1=1;
                 }
                  else 
                 {
                     $error1;
                 }
            }  

            if($f==1 && $f1==1)
            {
                $success="uploaded successfully";
                    $title = $_POST['pdftitle'];
                    //forimage upload
                       $img_name = $_FILES['imgupload']['name'];
                    $img_tmp = $_FILES['imgupload']['tmp_name'];
                    $path1 ="../product-pdf/".$img_name;
                    $str1=substr($path1, 3);
                      $s1= move_uploaded_file($img_tmp,$path1);
                      //for pdf upload
                    $file_name = $_FILES['pdfupload']['name'];
                    $file_tmp = $_FILES['pdfupload']['tmp_name'];
                    $path ="../product-pdf/".$file_name;
                    $str=substr($path, 3);
                      $s= move_uploaded_file($file_tmp,$path);
                    


                    $sql = "insert into pdfupload(title,pdfpath,pdfimage)values('".$title."','".$str."','".$str1."')";
                    $res = mysqli_query($con,$sql)or die("insert-error ".mysqli_error($con));
                    if($res){
                        //echo"<script> alert('image uploaded successfully')  ; </script>  ";
                    }
                    //echo"<script> window.location='imgupload.php' ; </script>  ";
            }

        }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
             <a href="imgupload.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php 
                                        echo $_SESSION['admin'];
                                       ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <p><?php 
                                        echo $_SESSION['admin'];
                                       ?> 
                                    </p>
                                </li>
                                
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php 
                                        echo $_SESSION['admin'];
                                       ?> <i class="caret"></i></span></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                         <li >
                            <a href="imgupload.php">
                                <i class="fa fa-dashboard"></i> <span>Gallery</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="product-upload.php">
                                <i class="fa fa-dashboard"></i> <span>product</span>
                            </a>
                        </li>
                        <li>
                            <a href="admincareers.php">
                                <i class="fa fa-th"></i> <span>Careers</span> 
                            </a>
                        </li>
                        <li>
                            <a href="admincontact.php">
                                <i class="fa fa-th"></i> <span>Contacts</span> 
                            </a>
                        </li>
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       UPLOAD NEW PDF
                       
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">General Elements</li>
                    </ol>
                </section>
                <div>
                                    <?php 
                                        if($f==1 && $f1==1)
                                            echo "<span id='successmsg' style='color:#86C724;font-size:16px;margin-left:20px;'>".$success."</span>";
                                        else
                                            echo "<span id='successmsg' style='color:red;font-size:16px;margin-left:20px;'>".$error."<br>".$error1."</span>"; 
                                    ?>
                                </div>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class=" col-xs-12">

      
    </div>
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Upload PDF</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form method="post"  role="form" enctype="multipart/form-data">
                                    <div class="box-body">
                                       
                                        <div class="form-group">
                                            <label for="pdftitle">Title</label>
                                            <input required type="text" class="form-control" name="pdftitle" id="pdftitle" placeholder="Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="InputFileimg">Select Image</label>
                                        <input required type="file" id="imgupload" name="imgupload">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="InputFilepdf">Select PDF</label>
                                        <input required type="file" id="pdfupload" name="pdfupload">
                                            
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-primary" id="submit">UPLOAD PDF</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->

                            

                        </div><!--/.col (left) -->
                       
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                              <div  name="successmsg1" id="successmsg1"  style="color: green;" > </div>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">DISPLAY UPLOADED PDF</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Title</th>
                                                 <th>PDF</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?PHP 
                                                $sql ="select id,title,pdfpath,pdfimage from pdfupload";
                                                $res =mysqli_query($con,$sql)or die("select-error ".mysqli_error($con));
                                                while($row = mysqli_fetch_array($res))
                                                {
                                                    $id=$row['id'];
                                            ?>
                                                <tr>
                                                <td><?php echo $id;  ?></td>
                                                <td><?php echo $row['title'];  ?></td>

                                                <td><a href="<?php echo '../'.$row['pdfpath'];  ?> " target="_blank"><?php echo substr($row['pdfpath'], 12);  ?>  </a> </td>
                                                 <td><img src="<?php echo '../'.$row['pdfimage'];  ?>" style="height: 80px;width: 80px;"> </td>
                                                <td><?php echo'<button class="btn-danger"  id='.$id.'  onclick="deleterec('.$id.')">Delete</button>';?></td>
                                               
                                            </tr>

                                            <?php 
                                                }
                                            ?>
                                           
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
         <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
         <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                
            });

            function deleterec(id)
        {
            var id= id;
            //alert(id);
            var str = "id="+id+"&status=deleteproduct";
            //alert(str);
            $.ajax({ type:'post',data:str,url:'ajaximgupload.php',success:function(html)
                {

                    //alert(html);
                    document.getElementById("successmsg1").innerHTML=html;
                }

            });
            
              //  var res = timedRefresh(1000);
           // if(res==false)
           // return false; 
}

        //function timedRefresh(timeoutPeriod) {
           //  setTimeout("location.reload(true);",timeoutPeriod);
        //}

                                    //setTimeout(function() {
                                   // $('#successmsg').fadeOut('fast');
                                   // }, 1000);

                             
                                

        </script>
    </body>
</html>