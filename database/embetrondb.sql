-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 09:47 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `embetrondb`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(10) NOT NULL,
  `name` varchar(245) NOT NULL,
  `email` varchar(245) NOT NULL,
  `contactno` bigint(11) NOT NULL,
  `subject` varchar(245) NOT NULL,
  `document` varchar(245) NOT NULL,
  `msg` varchar(245) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `name`, `email`, `contactno`, `subject`, `document`, `msg`, `createdOn`) VALUES
(1, 'Sungare', 'sungaretechnology@gmail.com', 7020914154, 'test', 'uploads/IMG_1849.jpg', 'test', '2018-06-04 21:57:32'),
(3, 'Sungare', 'sungaretechnology@gmail.com', 7020914154, 'tet', 'uploads/DSCN0035.JPG', 'ff', '2018-06-04 22:00:58'),
(6, 'Sungare', 'sungaretechnology@gmail.com', 7020914154, 'test', 'uploads/IMG_1849.jpg', 'test', '2018-06-05 01:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) NOT NULL,
  `name` varchar(245) NOT NULL,
  `email` varchar(245) NOT NULL,
  `subject` varchar(245) NOT NULL,
  `msg` varchar(245) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `msg`) VALUES
(4, 'Sungare', 'sungaretechnology@gmail.com', 'test', 'dd'),
(6, 'jasi', 'info@sungare.com', 'ss', 'tt'),
(7, 'ff', 'sungaretechnology@gmail.com', 'ff', 'ff');

-- --------------------------------------------------------

--
-- Table structure for table `imgupload`
--

CREATE TABLE `imgupload` (
  `id` int(10) NOT NULL,
  `title` varchar(245) NOT NULL,
  `imgpath` varchar(245) NOT NULL,
  `cretedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imgupload`
--

INSERT INTO `imgupload` (`id`, `title`, `imgpath`, `cretedOn`) VALUES
(4, 'dds', 'images/food.png', '2018-06-04 20:44:27'),
(5, 'dds', 'images/food.png', '2018-06-04 20:45:35'),
(6, 'ss', 'images/cater2.png', '2018-06-04 20:45:39'),
(7, 'test', 'images/jasita.png', '2018-06-04 20:46:13'),
(8, 'test', 'images/4d54451fa7d79ac9d8679367160b17cd.jpg', '2018-06-04 20:47:45'),
(12, 'dfsd', 'images/3ff072e248935b01ecf31e8f21fa361b.jpg', '2018-06-04 21:07:47'),
(13, 'test', 'images/CHANGE.jpg', '2018-06-04 21:11:31'),
(15, 'dfsd', 'images/food_diaries.jpg', '2018-06-04 21:27:43'),
(16, 'tsdt', 'images/38.png', '2018-06-04 22:29:32'),
(18, 'dfsd', 'images/b.jpg', '2018-06-04 23:50:44'),
(19, 'test', 'images/aa.jpg', '2018-06-05 06:32:54'),
(21, 'tsdt', 'images/black-and-white-photography-9-hd-wallpaper.jpg', '2018-06-05 03:25:19'),
(22, 'test', 'images/3ff072e248935b01ecf31e8f21fa361b.jpg', '2018-06-05 03:26:03');

-- --------------------------------------------------------

--
-- Table structure for table `pdfupload`
--

CREATE TABLE `pdfupload` (
  `id` int(10) NOT NULL,
  `title` varchar(245) NOT NULL,
  `pdfpath` varchar(245) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pdfimage` varchar(245) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pdfupload`
--

INSERT INTO `pdfupload` (`id`, `title`, `pdfpath`, `createdOn`, `pdfimage`) VALUES
(7, 'Catlog 1', 'product-pdf/polycab.pdf', '2018-06-05 05:13:01', 'product-pdf/catlog1.png'),
(8, 'Catlog 2', 'product-pdf/Solar PV Module.pdf', '2018-06-05 05:13:55', 'product-pdf/catlog2.png'),
(9, 'test', 'product-pdf/Solar PV Module.pdf', '2018-06-05 05:23:38', 'product-pdf/b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `userauth`
--

CREATE TABLE `userauth` (
  `id` int(10) NOT NULL,
  `email` varchar(245) NOT NULL,
  `password` varchar(245) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userauth`
--

INSERT INTO `userauth` (`id`, `email`, `password`, `createdOn`) VALUES
(1, 'info@sungare.com', 'reset@123', '2018-06-05 06:52:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imgupload`
--
ALTER TABLE `imgupload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pdfupload`
--
ALTER TABLE `pdfupload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userauth`
--
ALTER TABLE `userauth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `imgupload`
--
ALTER TABLE `imgupload`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pdfupload`
--
ALTER TABLE `pdfupload`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `userauth`
--
ALTER TABLE `userauth`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
